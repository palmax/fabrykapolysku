    <div class="section mcb-section" style="padding-top:40px; padding-bottom:40px; ">
        <div class="section_wrapper mcb-section-inner">
            <h3 style="">
                <span class="sportsman-gradient offerCategoryOtherTitle">Zobacz także:</span></h3>
            <div class="offerCategoryOthers">
                @foreach($items as $item)
                    <a href="{{route('offer_category.show.'.$item->id)}}" class="offerCategoryOthers__item">
                        <div class="offerCategoryOthers__imageWrap">
                            <img src="{{renderImage($item->galleryCover(), 500, 500, 'fit')}}" alt="" class="parter-img">
                        </div>
                        <span>{{$item->title}}</span>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
