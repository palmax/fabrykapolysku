@if($item)

    <div class="section mcb-section" style="padding-top:40px; padding-bottom:40px; ">
        <div class="section_wrapper mcb-section-inner">
            <h3 style="">
                <span class="sportsman-gradient offerCategoryOtherTitle">{{$item->title}}</span></h3>
            <div class="articleCategoryHome">
                <a href="{{$item->text}}" style="display: block">
                    <img src="{{renderImage($item->galleryCover(), 1920, 1080, 'resize')}}" alt="">
                </a>
            </div>
        </div>
    </div>


@endif
