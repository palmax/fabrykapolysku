@if($items->count() > 0)
    <ul id="menu-main-menu" class="menu menu-main">
        @foreach($items as $item)
            @php
                $isActive = false;
                $url = null;
                $target = '_self';

                // if($item->page) {
                //     $url = route($item->page->type);
                // }
                // else {
                    $url = url()->to($item->url);
                // }

                if($item->target) {
                    $target = $item->target;
                }

                $isActive = request()->fullUrlIs($url);
            @endphp
            <li class="link-id-{{$item->id}}{{$isActive ? ' current-menu-item' : ''}}">
                <a href="{{$url}}" target="{{$target}}"><span>{{$item->label}}</span></a>

                @if($item->id == 2)
                  @include('default.offer.category.list')
                @endif
            </li>
            @include('default.nav_item.main_items', ['items' => $item->navItems])
        @endforeach
    </ul>
@endif
