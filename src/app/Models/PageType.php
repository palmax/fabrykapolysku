<?php

namespace App\Models;

use App\Helpers\Enum;
use ReflectionClass;

abstract class PageType extends Enum
{
    /*
     * examples:
     * #1: controller_moduleCategory.action
     * #2: controller.action
     *
     * action = view
    */

    const INDEX_SHOW = [
        'name' => 'index.show',
        'module' => false,
        'fields' => [
//            'slider1' => ['head', 'Slider 1'],
//            'slider2' => ['head', 'Slider 1'],
//            'slider3' => ['head', 'Slider 1'],
//            'slider4' => ['head', 'Slider 1'],
//            'slider5' => ['head', 'Slider 1'],
//            'head1' => ['head', ''],
            'text1' => ['text', ''],
            'box1_1' => ['head', ''],
            'box1_2' => ['head', ''],
            'box2_1' => ['head', ''],
            'box2_2' => ['head', ''],
            'box3_1' => ['head', ''],
            'box3_2' => ['head', ''],
            'box4_1' => ['head', ''],
            'box4_2' => ['head', ''],
            'box5_1' => ['head', ''],
            'box5_2' => ['head', ''],
            'box6_1' => ['head', ''],
            'box6_2' => ['head', ''],
            'id_video_yt' => ['head', ''],
//            'head2' => ['head', ''],
//            'text2' => ['text', ''],
//            'text3' => ['text', 'Test'],
        ]
    ];
    const PAGE_SHOW = [
        'name' => 'page.show',
        'module' => false,
        'fields' => [
            'text1' => ['text', 'Text na stronie głównej']
        ]
    ];
    const ABOUT_US_SHOW = [
        'name' => 'about-us.show',
        'module' => false,
        'fields' => [
            'text1' => ['text', '']
        ]
    ];
    const CONTACT_SHOW = [
        'name' => 'contact.show',
        'module' => false,
        'fields' => [
            'pageTitle' => ['head', ''],

        ]
    ];

    const GALLERY_SHOW = [
        'name' => 'gallery.show',
        'module' => false,
        'fields' => [
            'pageTitle' => ['head', ''],
        ]
    ];
    const ARTICLE_INDEX = [
        'name' => 'article.index',
        'module' => true,
        'fields' => [
            'text1' => ['text', 'Text na stronie głównej']
        ]
    ];
    const ARTICLE_CATEGORY_INDEX = [
        'name' => 'article_category.index',
        'module' => true,
        'fields' => [
            'text1' => ['text', '']
        ]
    ];
    const OFFER_INDEX = [
        'name' => 'offer.index',
        'module' => true,
        'fields' => [
            'pageTitle' => ['head', ''],
            'text1' => ['text', '']
        ]
    ];
    const OFFER_CATEGORY_INDEX = [
        'name' => 'offer_category.index',
        'module' => true,
        'fields' => [
            'text1' => ['text', '']
        ]
    ];
    const REALIZATION_INDEX = [
        'name' => 'realization.index',
        'module' => true,
        'fields' => [
            'pageTitle' => ['head', ''],
        ]
    ];
    const REALIZATION_CATEGORY_INDEX = [
        'name' => 'realization_category.index',
        'module' => true,
        'fields' => [
            'pageTitle' => ['head', ''],
        ]
    ];


    static function getNames() : array {
        $class = new ReflectionClass(get_called_class());
        $types = array_values($class->getConstants());
        $names = [];
        foreach ($types as $type) {
            $names[] = $type['name'];
        }
        return $names;
    }

    static function getByName($name) : array {
        $class = new ReflectionClass(get_called_class());
        $types = array_values($class->getConstants());
        foreach ($types as $type) {
            if($type['name'] == $name)
                return $type;
        }
//        return $names;
    }
}
