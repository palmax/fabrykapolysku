<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_column">
                  <div class="column_attr clearfix">
                    <h2 style="font-size: 22px; line-height: 1.15; margin-bottom: 10px; margin-top: 1rem;"><span class="sportsman-gradient">Fabryka Połysku Sędziszów Małopolski</span></h2>
                    <div class="big" style="color:#000000">
                      <p style="padding-left: 0">Niebywałą gratka dla fanów motoryzacji będą bez wątpienia usługi, które oferujemy w Fabryce Połysku w Sędziszowie Małopolskim. Zabezpieczymy twój pojazd, dzięki czemu będzie wyglądał jak nowy. Nasze studio jest miejscem, gdzie samochody naszych klientów przechodzą całkowitą metamorfozę. Jeśli i Ty chcesz odmienić swój ukochany samochód, to poznaj oferowane przez nas usługi.</p>
                    </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_39_348ODemwt8qdYDD8TBodjioFXvpJaf3vOvI9enOd.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_19_dSM0g83ihuyDF6gdCzAro3UMTRuUv97BSc4iE5Ib.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
        <div class="mcb-wrap-inner">
            <div class="column mcb-column one column_column">
                <div class="column_attr clearfix">
                  <p><strong>Folia ochronna PPF Sędziszów Małopolski – zabezpiecz lakier przed uszkodzeniem!</strong></p>

                  <p>W naszym studio zajmujemy się zabezpieczeniem elementów karoserii samochodowej, które są najbardziej narażone na uszkodzenia. Do tego celu wykorzystujemy wysokiej klasy folię PPF, która pozwala na kompleksowe i skuteczne wykonanie usługi. Nasi klienci mogą wybrać folię PPF matową lub z połyskiem. Choć zdecydowana większość wybiera klasyczne rozwiązania, to nie brakuje również fanów motoryzacji, którzy chcą cieszyć się mroźnym efektem, który możliwy jest dzięki wykorzystaniu folii matowej.</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
    <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_column">
            <div class="column_attr clearfix">
              <p><strong>Ceramiczne powłoki ochronne Sędziszów Małopolski – zabezpiecz lakier przed korozją, brudem i promieniowaniem UV!</strong>&nbsp;</p>

              <p>Częste czyszczenie samochodu potrafi utrudnić życie niejednego kierowcy. Dlatego wielu z nich zastanawia się, jak zabezpieczyć samochód, aby uzyskać perfekcyjny stan lakieru przez długi czas. Oferowane przez nas ceramiczne powłoki ochronne to najskuteczniejszy sposób na utrzymanie długotrwałego efektu, który pozostanie nawet przez kilkanaście miesięcy. Korzystając z naszych usług, zyskasz:</p>
              <ul>
                <li>perfekcyjny wygląd samochodu,</li>
                <li>więcej czasu, który musiałbyś poświęcić na częste mycie samochodu,</li>
                <li>kompleksową ochronę lakieru.</li>
              </ul>
              <p><strong>Ceramika na auto w Sędziszowie Małopolskim</strong> to wysokiej jakości usługi, które pozwolą na zabezpieczenie ukochanego pojazdu.</p>
            </div>
          </div>
      </div>
    </div>
    <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_image">
              <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                  <div class="image_wrapper">
                      <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_20_QIII5YtgjetulBHxoSEOdkonKDKlxDhUth1EIkK0.jpeg">
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_14_Fs9w6nOdWD9vgHKW2TJWmTLQDrxr34yHi6dRZ8Pm.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
        <div class="mcb-wrap-inner">
            <div class="column mcb-column one column_column">
                <div class="column_attr clearfix">
                  <p><strong>Polerowanie samochodu Sędziszów Małopolski – lakier jak po wyjeździe z salonu? Z nami to możliwe!</strong>&nbsp;</p>

                  <p>Usuwanie rys z lakieru samochodowego to nie lada wyzwanie, którego nie warto podejmować się na własną rękę. Fabryka Połysku to studio, w którym wykonamy profesjonalne <strong>polerowanie samochodu w Sędziszowie Małopolskim</strong>, dzięki któremu rysy z lakieru samochodowego odejdą w niepamięć.</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
    <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_column">
            <div class="column_attr clearfix">
              <p><strong>Korekta lakieru Sędziszów Małopolski</strong></p>

              <p>Jak usuwamy rysy z lakieru? Oferujemy naszym klientom kilka wariantów usług. Oto one:</p>
              <ul>
                <li>jednoetapowa korekta lakieru – to najprostszy wariant pozwalający na usunięcie 70% zarysowań,</li>
                <li>dwu lub trzyetapowa korekta lakieru – złożona jest z cięcia i polerowania usuwającego nawet 90% niedoskonałości,</li>
                <li>wieloetapowa korekta lakieru – szlifowanie lakieru, cięcie i polerowanie, które ma aż 99% skuteczności,</li>
                <li>nabłyszczanie – usuwa aż 50% rys z lakieru samochodowego.</li>
              </ul>
              <p>Idealny stan lakieru w Twoim samochodzie? To jest możliwe, jeśli tylko skorzystasz z naszych usług!</p>
            </div>
          </div>
      </div>
    </div>
    <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_image">
              <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                  <div class="image_wrapper">
                      <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_38_VikC0JjD2b1OYXSdfYjdFGrgEI6UzzFp9Wp0aKYe.jpeg">
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_47_fThHS0sqr9XUfu2CwdNBQRMKpUDDTLP8skD9KcD5.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
        <div class="mcb-wrap-inner">
            <div class="column mcb-column one column_column">
                <div class="column_attr clearfix">
                  <p><strong>Autodetailing Sędziszów Małopolski – odświeżymy każde wnętrze samochodowe!</strong>&nbsp;</p>

                  <p>Oprócz kompleksowych usług dotyczących renowacji lakieru samochodowego w Fabryce Połysku oferujemy również <strong>detailing Sędziszów Małopolski</strong>. Te profesjonalne usługi mają na celu oczyszczenie i odnowienie wnętrza samochodu. Oferowane przez nas warianty usług to:</p>
                  <ul>
                    <li>detailing podstawowy – odkurzanie, mycie szyb, mycie wycieraczek i czyszczenie plastików,</li>
                    <li>detailing rozszerzony – odkurzanie, mycie szyb, mycie wycieraczek, czyszczenie plastików, wydmuchiwanie ze szczelin i zakamarków, czyszczenie i dressing plastików, dressing wycieraczek, czyszczenie drzwi i perfumowanie wnętrza,</li>
                    <li>detailing pełny – odkurzanie, mycie szyb, mycie wycieraczek, czyszczenie plastików, wydmuchiwanie ze szczelin i zakamarków, czyszczenie i dressing plastików, dressing wycieraczek, czyszczenie drzwi, perfumowanie wnętrza, pranie tapicerki, pranie podsufitki, branie bagażnika i ozonowanie.</li>
                  </ul>
                  <p>Tylko Fabryka Połysku w Sędziszowie Małopolskim oferuje usługi na tak wysokim poziomie!</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>