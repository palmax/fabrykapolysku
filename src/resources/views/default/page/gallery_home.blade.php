<div class="section mcb-section" style="padding-top:40px; padding-bottom:40px; ">
    <div class="section_wrapper mcb-section-inner">
        <h3 style="">
            <span class="sportsman-gradient offerCategoryOtherTitle" data-aos-offset="-300" data-aos="fade-top">Zobacz naszą galerię</span></h3>
        <div class="offerCategoryOthers">

        @foreach($items as $item)
                <a href="{{route('gallery.show')}}" class="offerCategoryOthers__item" data-aos-offset="-400" data-aos="fade-up">
                    <div class="offerCategoryOthers__imageWrap">
                        <img src="{{renderImage($item->url, 500, 600, 'fit')}}" alt="" class="parter-img">
                    </div>
                    <span>{{$item->title}}</span>
                </a>
            @endforeach
        </div>
        <a class="button button_size_3 button_js" style="display: block; margin: 0 auto; text-align: center; width: fit-content; padding: .1rem 2rem" href="{{route('gallery.show')}}"><span class="button_label">GALERIA</span></a>
    </div>
</div>
