<?php

namespace App\Http\Requests\Admin;

use App\Forms\Admin\ArticleForm;
use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        foreach (ArticleForm::FIELDS as $field) {
            $rules['article.'.$field['name']] = $field['rules'];
        }

        return $rules;
    }
}
