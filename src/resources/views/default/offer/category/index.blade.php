@extends('default.layout')
@section('content')


<section class="offerIndex">
    <div class="container">
        <div class="row justify-content-center">
        @foreach($items as $key=>$item)
<div class="col-sm-9 col-md-6 col-lg-4 mb-3" data-aos-offset="-150" data-aos="fade-top" data-aos-duration="{{($key+1)*200}}">
    <div class="offerIndex__item">

                <div class="offerIndex__image">

                    <img src="{{renderImage($item->galleryCover(), 400, 300, 'fit')}}" alt="{{$item->title}}">

                </div>


                <div class="offerIndex__text">
                    <h2 class="offerIndex__title">
                        <span class="sportsman-gradient"> {{$item->title}} </span>
                    </h2>
                    <div class="offerIndex__lead">
                        {!! $item->lead !!}
                    </div>

                    <a class="button button_size_3 button_js" href="{{route('offer_category.show.'.$item->id)}}"><span class="button_label">ZOBACZ OFERTĘ</span></a>


        </div>

    </div>
</div>

        @endforeach
        </div>
    </div>
</section>

@include('default.page.gallery_home')

@include('default.realization.category.slider')



{{--    <h1>Offers</h1>--}}

{{--    <div>{!! $fields->text1 !!}</div>--}}

{{--    @foreach($items as $item)--}}
{{--        <a href="{{route('offer.show.'.$item->id)}}">--}}
{{--            <h2>{{$item->title}}</h2>--}}
{{--            <p>{!! $item->lead !!}</p>--}}
{{--        </a>--}}
{{--    @endforeach--}}

{{--    {!! $items->links() !!}--}}

@endsection
