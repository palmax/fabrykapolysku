@extends('default.layout')
@section('content')

    <div class="widgets_wrapper contact-wrap" style="padding:70px 0;">
        <div class="container">
            <div class="column one">
                {{--                <div style="padding-bottom: 100px; padding-top: 50px">--}}
                {{--                    <img src="{{asset('images/home.svg')}}" alt=""--}}
                {{--                         style="display: block; margin: 0 auto 1rem; width: 5rem">--}}
                {{--                    <h3 class=" text-center item-contact" style="line-height: 1.4; text-transform: uppercase"><a--}}
                {{--                            href="{{getConstField('google_map')}}">{{getConstField('company_city')}}--}}
                {{--                            <br>{{getConstField('company_address')}}--}}
                {{--                            <br> {{getConstField('company_post_code')}} {{getConstField('company_city')}}</a></h3>--}}
                {{--                    <img src="{{asset('images/phone.svg')}}" alt=""--}}
                {{--                         style="display: block; margin: 0 auto 1rem; width: 5rem">--}}
                {{--                    <h3 class=" text-center item-contact -phone"><a href="Tel:{{str_replace(' ', '', getConstField('phone'))}}"--}}
                {{--                                                             style="">{{getConstField('phone')}}</a></h3>--}}
                {{--                    <h3 class=" text-center item-contact -email"><a--}}
                {{--                            href="Tel:{{getConstField('email')}}">{{getConstField('email')}}</a></h3>--}}
{{--            </div>--}}


            <div style="padding-bottom: 100px; padding-top: 50px">
{{--                <img src="{{asset('images/home.svg')}}" alt=""--}}
{{--                     style="display: block; margin: 0 auto 1rem; width: 5rem">--}}
                <h3 class=" text-center item-contact -phone"><a
                        href="Tel:{{str_replace(' ', '', getConstField('phone'))}}"
                        style="color: #FF0001"> <i class="fas fa-phone-alt"></i> {{getConstField('phone')}}</a></h3>

{{--                <img src="{{asset('images/phone.svg')}}" alt=""--}}
{{--                     style="display: block; margin: 0 auto 1rem; width: 5rem">--}}

                <h3 class=" text-center item-contact -email">
                    <i class="fas fa-envelope"></i>
                    <a
                        href="Tel:{{getConstField('email')}}">
                        {{getConstField('email')}}
                    </a></h3>
                <div class="contact-social" style="margin-bottom: 5rem">
                    <a href="{{getConstField('facebook_url')}}"><img src="{{asset('images/facebook.png')}}" alt=""></a>
                    <a href="{{getConstField('instagram_url')}}"><img src="{{asset('images/instagram.png')}}" alt=""></a>
                </div>
                <h3 class=" text-center item-contact -location" style="line-height: 1.4; text-transform: uppercase; margin-bottom: 3rem">
                    <div style="display: flex; align-items: center"><i style="color: #ff0001" class="fas fa-map-marker-alt"></i> <p style="margin-left: 7px;color: #ff0001">Oddział {{getConstField('company_city')}}</p></div>
                    <a href="{{getConstField('google_map')}}">
                        {{getConstField('company_city')}}
                        {{getConstField('company_address')}}
                        <br> {{getConstField('company_post_code')}} {{getConstField('company_city')}},</a>
                </h3>
                <h3 class=" text-center item-contact -location" style="line-height: 1.4; text-transform: uppercase; margin-bottom: 5rem">
                    <div style="display: flex; align-items: center"><i style="color: #ff0001" class="fas fa-map-marker-alt"></i> <p style="margin-left: 7px;color: #ff0001">Oddział {{getConstField('company_city_2')}}</p></div>
                    <a href="{{getConstField('google_map_2')}}">{{getConstField('company_address_2')}} <br> {{getConstField('company_post_code_2')}} {{getConstField('company_city_2')}}</a>
                </h3>
                <div style="margin-bottom: 5rem">
                    <h3 class=" text-center item-contact -location">
                    <div style="display: flex; align-items: center"><i style="color: #ff0001" class="fas fa-map-marker-alt"></i> <p style="margin-left: 7px;color: #ff0001">Oddział {{getConstField('company_city')}}</p></div>
                    </h3>
                        <iframe data-aos-offset="-150" data-aos="fade-top" src="{{getConstField('google_map_iframe')}}" width="100%" height="450"
                            style="border:0;" allowfullscreen="" loading="lazy"></iframe>

                </div>

                <div style="margin-bottom: 3rem">
                    <h3 class=" text-center item-contact -location">
                        <div style="display: flex; align-items: center"><i style="color: #ff0001" class="fas fa-map-marker-alt"></i> <p style="margin-left: 7px;color: #ff0001">Oddział {{getConstField('company_city_2')}}</p></div>
                    </h3>
                    <iframe data-aos-offset="-150" data-aos="fade-top" src="{{getConstField('google_map_iframe_2')}}" width="100%" height="450"
                            style="border:0;" allowfullscreen="" loading="lazy"></iframe>

                </div>
            </div>

            <aside class="widget_text widget widget_custom_html">
                <div class="textwidget custom-html-widget">
                    <div style="text-align: center;">
                        <h5 class="sportsman-gradient" style="margin-bottom: 5px;">MASZ JAKIEŚ PYTANIE?</h5>
                        <h3>NAPISZ DO NAS!</h3>
                        <hr class="no_line" style="margin:0 auto 15px">
                        <div class="form-margin">

                            <div id="mc4wp-form-1" class="form mc4wp-form">
                                @include('default.form.contact_form')
                            </div>


                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>
    </div>

    @include('default.realization.category.slider')

@endsection
