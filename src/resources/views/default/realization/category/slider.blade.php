@if(count($items)>0)

    <div class="section mcb-section" style="padding-top:40px; padding-bottom:40px; ">
        <div class="section_wrapper mcb-section-inner">
            <h3 style="font-size: 1.5rem">
                <span class="sportsman-gradient products-title">Korzystamy z najlepszych producentów:</span></h3>
            <div class="partners">
                @foreach($items[0]->gallery->items as $galleryItem)
                    <div class="wrap mcb-wrap one-sixth valign-top clearfix" style="height: 100%; display: flex; align-content: center; justify-content: center; padding: 0 1rem">
                        <img src="{{renderImage($galleryItem->url, 200, 200, 'resize')}}" alt="" class="parter-img">
                    </div>
                @endforeach
            </div>

        </div>
    </div>

@endif
@push('scripts.body.bottom')
    <script>
        jQuery(document).ready(function (){
            jQuery('.partners').slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1000,
                arrows: false,
                dots: false,
                responsive: [
                    {
                        breakpoint: 960,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 3
                        }
                    }
                ]
            });
        })
    </script>
@endpush
