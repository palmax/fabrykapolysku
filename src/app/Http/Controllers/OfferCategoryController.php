<?php


namespace App\Http\Controllers;


use App\Forms\Admin\Admin\PageForm;
use App\Http\Requests\PageRequest;
use App\Models\Offer;
use App\Models\OfferCategory;
use App\Models\Page;
use App\Models\Realization;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class OfferCategoryController extends Controller
{
    public function show($item) {
        SEOMeta::setTitle($item->seo->title);
        SEOMeta::setDescription($item->seo->description);
        SEOMeta::addKeyword($item->seo->tags);

        $offerItems = Offer::with([])
            ->activeAndLocale()
            ->where('offer_category_id', '=', $item->id)
            ->get();

        $priceList = Realization::with([])
            ->where('id', '=', $item->price_list_id)
            ->first();

        if (!(is_null($priceList)))
            $priceList = $priceList->text;
        else {
            $priceList = '';
        };

        $categories = OfferCategory::with([])
            ->activeAndLocale()
            ->where('id', '!=', $item->id)
            ->get();



        return view('default.offer.category.show', ['item' =>compact('item'), 'offerItems' => $offerItems, 'priceList' => $priceList, 'categories' => $categories]);
    }

    public function index($view) {
        $items = OfferCategory::with([])
            ->activeAndLocale()
            ->get();
        $view->items = $items;
    }

    public function home($view){
        $items = OfferCategory::with([])
            ->activeAndLocale()
            ->get();
        $view->items = $items;
    }
}
