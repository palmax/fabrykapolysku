

<form id="contactForm" method="POST">
    <div class="form-group">
        <label for="name">Imię i nazwisko</label>
        <input id="name" type="text" name="name" placeholder="Imię i nazwisko" class="form-control">
        <div class="invalid-feedback"></div>
    </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input id="email" type="email" name="email" placeholder="Email" class="form-control">
                <div class="invalid-feedback"></div>
            </div>
            <div class="form-group">
                <label for="phone">Numer telefonu</label>
                <input id="phone" type="text" name="phone" placeholder="Numer telefonu" class="form-control">
                <div class="invalid-feedback"></div>

    </div>
    <div class="form-group">
        <label for="message">Treść wiadmości</label>
        <textarea id="message" name="message" rows="5" placeholder="Treść wiadmości" class="form-control"></textarea>
        <div class="invalid-feedback"></div>
    </div>

    <div class="form-group">
        <div class="form-check d-flex">
            <input id="rule" type="checkbox" name="rule" placeholder="Rule" class="form-check-input">
            <label for="rule" class="form-check-label">{!! getConstField('contact_form_rule') !!}</label>
            <div class="invalid-feedback"></div>
        </div>
    </div>

    <div class="form-group">
        <div class="g-recaptcha" data-sitekey="{{$siteKey}}"></div>
        <div class="invalid-feedback"></div>
    </div>

    <button type="submit" value="Send" class="button text-uppercase" style="background-color: #ff0001; font-weight: 600">Wyślij wiadomość</button>

    <div id="alert" class="alert"></div>
</form>



@push('scripts.body.bottom')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <script>
        document.getElementById('contactForm').addEventListener('submit', e => {
            e.preventDefault();
            submitForm(e.target);
        })
    </script>
@endpush
