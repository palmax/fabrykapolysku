<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_column">
                  <div class="column_attr clearfix">
                    <h2 style="font-size: 22px; line-height: 1.15; margin-bottom: 10px; margin-top: 1rem;"><span class="sportsman-gradient">Fabryka Połysku Mielec</span></h2>
                    <div class="big" style="color:#000000">
                      <p style="padding-left: 0">Jeśli chcesz zadbać o swój samochód, to Fabryka Połysku w Mielcu będzie do tego najlepszym miejscem. Oferowane przez nas usługi mają na celu przedłużenie idealnego wyglądu każdego samochodu. Z nami Twój pojazd przejdzie profesjonalna i dogłębną metamorfozę. Bo ważny jest nie tylko wygląd zewnętrzny, ale również wnętrze samochodu. Nasze studio to miejsce z pasją, w którym do każdego klienta podchodzimy indywidualnie.  Skorzystaj z naszych usług i cieszą się idealnym stanem swojego samochodu.</p>
                    </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_39_348ODemwt8qdYDD8TBodjioFXvpJaf3vOvI9enOd.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_19_dSM0g83ihuyDF6gdCzAro3UMTRuUv97BSc4iE5Ib.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
        <div class="mcb-wrap-inner">
            <div class="column mcb-column one column_column">
                <div class="column_attr clearfix">
                  <p><strong>Folia ochronna PPF Rzeszów - zabezpiecz swój samochód!</strong></p>

                  <p>Wykorzystywana przez nas <strong>Folia ochronna PPF Mielec</strong> to doskonałe zabezpieczenie elementów samochodowych. W Fabryce Połysku używamy wyłącznie wysokiej jakości produktów, które zapewniają skuteczne zabezpieczenie i pielęgnacje pojazdu. <strong>Zabezpieczenie lakieru w Mielcu</strong> odbywa się poprzez oklejenie folią PPF lakieru samochodowego w najbardziej narażonych na uszkodzenie miejscach. Do wyboru mamy folię z połyskiem, a także folię matową, dzięki której na lakierze samochodowym pojawia się oryginalny efekt.</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
    <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_column">
            <div class="column_attr clearfix">
              <p><strong>Ceramiczne powłoki ochronne Mielec - bardzo trwałe zabezpieczenie lakieru samochodowego</strong>&nbsp;</p>

              <p>Częste woskowanie samochodu, to popularny sposób na zabezpieczenie lakieru. Niestety jest to bardzo pracochłonna czynność, a do tego przynosi krótkotrwały efekt, dlatego w Fabryce Połysku proponujemy zupełnie inne rozwiązanie. <strong>Ceramika na auto Mielec</strong> to trwały i skuteczny sposób, który zabezpieczy lakier Twojego samochodu nawet na kilka lat! Korzystając z naszych usług, zyskasz czas na swoje przyjemności, a do tego:</p>
              <ul>
                <li>zapewnisz ochronę lakieru przed niekorzystnym wpływem promieniowania UV,</li>
                <li>zabezpieczysz lakier przed korozją i mikro zarysowaniami,</li>
                <li>przeznaczysz mniej czasu na czyszczenie samochodu, ponieważ ceramiczna powłoka zabezpiecza lakier również przed brudem.</li>
              </ul>
              <p>Jeśli szukasz trwałości i wysokiej jakości świadczonych usług, to skorzystaj z naszej oferty i umów się na wizytę!</p>
            </div>
          </div>
      </div>
    </div>
    <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_image">
              <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                  <div class="image_wrapper">
                      <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_20_QIII5YtgjetulBHxoSEOdkonKDKlxDhUth1EIkK0.jpeg">
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_14_Fs9w6nOdWD9vgHKW2TJWmTLQDrxr34yHi6dRZ8Pm.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
        <div class="mcb-wrap-inner">
            <div class="column mcb-column one column_column">
                <div class="column_attr clearfix">
                  <p><strong>Polerowanie samochodu Mielec - pozbądź się nieestetycznych rys!</strong>&nbsp;</p>

                  <p>Usunięcie rys z lakieru samochodowego jest niebywale trudne, dlatego nie warto robić tego samodzielnie. Profesjonalne <strong>polerowanie samochodu w Mielcu</strong> to idealne zadanie dla nas!</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
    <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_column">
            <div class="column_attr clearfix">
              <p><strong>Korekta lakieru Mielec</strong>&nbsp;</p>

              <p>Korekta lakieru w Mielcu to usługa, którą wykonamy w naszym studio. Naszym klientom do wyboru proponujemy 3 warianty tej usługi, które są fantastycznym sposobem na odświeżenie wyglądu każdego samochodu. Poniżej trzy etapy związane z korektą lakieru:</p>
              <ul>
                <li>korekta lakieru jednoetapowa polega na usuwanie 70% rys w krótkim czasie,</li>
                <li>korekta lakieru dwuetapowa to ciecie, a następnie polerowanie, które jest w stanie usunąć nawet 90% zarysowań,</li>
                <li>nabłyszczanie to najprostszy sposób usuwania rys z lakieru samochodowego i ma on 50% skuteczności.</li>
              </ul>
              <p>Profesjonalne polerowanie lakieru samochodowego w Twoim pojeździe jest możliwe dzięki nam! Skorzystaj z naszych usług i wybierz wariant, który Cię najbardziej interesuje, a my zadbamy o Twój samochód.</p>
            </div>
          </div>
      </div>
    </div>
    <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_image">
              <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                  <div class="image_wrapper">
                      <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_38_VikC0JjD2b1OYXSdfYjdFGrgEI6UzzFp9Wp0aKYe.jpeg">
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_47_fThHS0sqr9XUfu2CwdNBQRMKpUDDTLP8skD9KcD5.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
        <div class="mcb-wrap-inner">
            <div class="column mcb-column one column_column">
                <div class="column_attr clearfix">
                  <p><strong>Autodetailing Mielec - sprawimy, że wnętrze Twojego samochodu będzie jak nowe!</strong>&nbsp;</p>

                  <p>Z pewnością już wiesz, że uzyskanie idealnego lakieru bez ani jednej ryski, jest możliwe, jeśli skorzystasz z naszych usług. To nie wszystko! Nasza oferta jest o wiele bardziej rozbudowana, a nasze usługi skierowane są do miłośników samochodowych, którym zależy na idealnym stanie samochodu nie tylko od zewnątrz, ale również w jego wnętrzu. <strong>Detailing Mielec</strong> sprawi, że Twoje auto będzie kompleksowo zaopiekowane, a po wyjeździe z naszego studio będzie wyglądało, jakby dopiero wyjechało z salonu samochodowego.</p>
                  <p>Fabryka Połysku w Mielcu to studio, w którym oferujemy autodetailing opierający się na trzech różnych wariantach, do których zaliczamy:</p>
                  <ul>
                    <li>podstawowy <strong>detailing Mielec</strong> - usługa ta składa się z czyszczenia plastików, odkurzania, mycia wycieraczek, a także mycia szyb,</li>
                    <li>rozszerzony <strong>detailing Mielec</strong> - oprócz czyszczenia plastików mycia wycieraczek, szyb i odkurzania wnętrza, w skład usługi wchodzą: wydmuchiwanie ze szczelin i zakamarków, czyszczenie i dressing plastików, dressing wycieraczek, kompleksowe czyszczenie drzwi i perfumowanie wnętrza,</li>
                    <li>pełny <strong>detailing Mielec</strong> - w skład tego wariantu wchodzą wszystkie wcześniejsze usługi i zostają one poszerzone o: pranie tapicerki i podłogi, dokładne pranie bagażnika, odkurzanie, czyszczenie podsufitki i ozonowanie.</li>
                  </ul>
                  <p>Jeśli zależy Ci na najwyższym poziomie usług, to zgłoś się do naszego studia w Mielcu i skorzystaj z usług autodetailingu. Pozwól się zaskoczyć, a my profesjonalnie odmienimy wnętrze Twojego samochodu. Nie czekaj ani chwili i wykonaj pierwszy krok!</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>