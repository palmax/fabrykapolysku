@extends('default.layout')

@php($pageTitle = 'OFERTA')
@section('content')
    <div class="section mcb-section bg-cover offer-wrap "
         style="padding-top:190px; padding-bottom:190px; background-color:transparent;">
        <div class="section_wrapper mcb-section-inner">
            <div class="wrap mcb-wrap two-fifth valign-top move-up clearfix" style="margin-top:-160px"
                 data-mobile="no-up">
                <div class="mcb-wrap-inner">
                    <div class="column mcb-column one column_image">
                        <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                            <div class="image_wrapper">
                                <img class="scale-with-grid"
                                     src="{{renderImage($item['item']->galleryCover(), 780,1000, 'resize')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrap mcb-wrap three-fifth valign-top clearfix no-padding-mobile" style="padding:0 0 0 4%">
                <div class="mcb-wrap-inner">
                    <div class="column mcb-column one-sixth column_image">
                        <div class="image_frame image_item no_link scale-with-grid no_border">
                            <div class="image_wrapper">
                                <img class="scale-with-grid" src="{{asset('images/line.png')}}">
                            </div>
                        </div>
                    </div>
                    <div class="column mcb-column five-sixth column_column">
                        <div class="column_attr clearfix offer-main-text">
                            <h2 style="color:#000000"><span class="sportsman-gradient">{!! $item['item']->title !!}</h2>
                            <div class="big" style="color:#000000">
                                {!! $item['item']->lead !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($offerItems as $key => $offerItem)
        @if(!($offerItem->galleryCover() == ''))
            @if($key%2 == 0)
                <div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column">
                                    <div class="column_attr clearfix">
                                        {!! $offerItem->text !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-top move-up clearfix" style="margin-top:-150px"
                             data-mobile="no-up">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                        <div class="image_wrapper">
                                            <img class="scale-with-grid"
                                                 src="{{renderImage($offerItem->galleryCover(), 780, 1000, 'resize' )}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;;">
                    <div class="section_wrapper mcb-section-inner">
                        <div class="wrap mcb-wrap one-second valign-top clearfix" style="padding-bottom: 20px">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_image">
                                    <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                                        <div class="image_wrapper">
                                            <img class="scale-with-grid"
                                                 src="{{renderImage($offerItem->galleryCover(), 780, 1000, 'resize' )}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrap mcb-wrap one-second valign-top clearfix" style="padding:0 0 0 4%">
                            <div class="mcb-wrap-inner">
                                <div class="column mcb-column one column_column">
                                    <div class="column_attr clearfix">
                                        {!! $offerItem->text !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @else
            <div class="section mcb-section offer -black" style="padding-top:50px; padding-bottom:50px; background: #000000; color: #ffffff">
                <div class="section_wrapper mcb-section-inner">
                    <div class="wrap mcb-wrap one valign-top clearfix">
                        <div class="wrap mcb-wrap one valign-top clearfix offer" style="padding:0 0 0 4%">
                            <div class="mcb-wrap-inner">
                                <div class="wrap mcb-wrap one valign-top clearfix" style="padding:0 0 0 4%">
                                    <div class="mcb-wrap-inner">
                                        <div class="column mcb-column one column_column" style="margin-bottom: 0">
                                            <div class="column_attr clearfix text-center">
                                                {!! $offerItem->text !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif


    @endforeach

{{--
    @if($priceList)
        <div class="section mcb-section offer priceList" style="padding-top:50px; padding-bottom:0;">
            <div class="section_wrapper mcb-section-inner">
                <div class="wrap mcb-wrap one valign-top clearfix" style="padding:0 4% 0 0">
                    <div class="mcb-wrap-inner">
                        <div class="column mcb-column one column_column">
                            <div class="column_attr clearfix p-no-margin priceText">
                                <h2><span class="sportsman-gradient">CENNIK OFERTY:</span></h2>
                                {!! $priceList !!}
                                <br>Podane ceny są cenami netto.

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
--}}
    @include('default.offer.category.other', ['items'=>$categories])

    @include('default.realization.category.slider')


    {{--            <section class="priceLit">--}}
    {{--                <div class="container">--}}
    {{--                </div>--}}
    {{--            </section>--}}


    <div class="offerCategoryFixed">

        <div class="offerCategoryFixed__items">
            @foreach($categories as $category)
                <a class=" text-uppercase" href="{{route('offer_category.show.'.$category->id)}}">{{$category->title}}</a>
            @endforeach
        </div>
        <div class="offerCategoryFixed__showAll" onclick="showOrHidden(this)">
            <span class=" text-uppercase">ZOBACZ INNE USŁUGI</span>
        </div>
    </div>

@endsection


@push('scripts.body.bottom')
    <script>
        const fixedCategory = jQuery('.offerCategoryFixed');
        const list = jQuery('.offerCategoryFixed__items');
        const textButton = jQuery('.offerCategoryFixed__showAll span')

        fixedCategory.css('transform', 'translateY(-50%) translateX(-'+ list.outerWidth(true) +'px)');
        let show = false;
        function showOrHidden(){

           if(!show){
               fixedCategory.css('transform', 'translateY(-50%) translateX(0)');
               show = true;
               textButton.text('SCHOWAJ LISTE')
           }else {
               fixedCategory.css('transform', 'translateY(-50%) translateX(-'+ list.outerWidth(true) +'px)');
               show = false;
               textButton.text('ZOBACZ INNE USŁUGI')

           }
        }

        function eventResize(){
            fixedCategory.css('transform', 'translateY(-50%) translateX(-'+ list.outerWidth(true) +'px)');
            show = false;
        }
        window.addEventListener('resize', eventResize);
    </script>
@endpush
