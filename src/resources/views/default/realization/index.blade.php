

    @extends('default.layout')
@section('content')

    <div class="priceList-wrap" style="padding: 50px 0 0">
    @foreach($items as $item)
        <div class="section mcb-section offer priceList" style="padding-top: 0; padding-bottom:0;" data-aos-offset="-150" data-aos="fade-top">
            <div class="section_wrapper mcb-section-inner">
                <div class="wrap mcb-wrap one valign-top clearfix" style="padding:0 0 0 0">
                    <div class="mcb-wrap-inner">
                        <div class="column mcb-column one column_column">
                            <div class="column_attr clearfix">
                                <h4><span class="sportsman-gradient">{!! $item->title !!}</span></h4>
                                {!! $item->text !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
{{--        <div class="section mcb-section offer priceList" style="padding-top: 0; padding-bottom:20px;">--}}
{{--            <div class="section_wrapper mcb-section-inner">--}}
{{--         <small>Podane ceny są cenami netto.</small>--}}
{{--                <br>--}}
{{--            </div>--}}
{{--        </div>--}}

</div>

    @include('default.realization.category.slider')

@endsection


