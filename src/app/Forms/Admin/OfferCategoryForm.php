<?php


namespace App\Forms\Admin;


use App\Helpers\Form;
use App\Models\OfferCategory;
use App\Models\BaseModel;
use App\Models\Page;
use App\Models\PageType;
use App\Models\Realization;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class OfferCategoryForm extends Form
{

    const FIELDS = [
        'title' => [
            'name' => 'title',
            'type' => 'text',
            'label' => 'admin.offer_category.title',
            'rules' => ['max:255', 'min:2', 'required'],
            'attrs' => [
                'seoUrl' => true,
                'seoTitle' => true,
            ]
        ],
        'price_list_id' => [
            'name' => 'price_list_id',
            'type' => 'select',
            'label' => 'admin.offer_category.price',
            'rules' => [],
            'options' => [],
        ],
        'lead' => [
            'name' => 'lead',
            'type' => 'textarea',
            'label' => 'admin.offer_category.lead',
            'class' => 'ckeditorStandard',
            'rules' => [],
            'options' => [],
        ],
        'text' => [
            'name' => 'text',
            'type' => 'textarea',
            'label' => 'admin.offer_category.text',
            'class' => 'ckeditorStandard',
            'rules' => [],
            'options' => [],
        ],
        'active' => [
            'name' => 'active',
            'type' => 'checkbox',
            'label' => 'admin.active',
            'rules' => [],
            'options' => [],
        ],
    ];

    public function __construct($model = null)
    {
        foreach (self::FIELDS as $name => $field) {
            $this->modelFields[$name] = $field;
        }

        $priceLists = Realization::with([])->adminLocale()->get();
        foreach ($priceLists as $priceList) {
            $this->modelFields['price_list_id']['options'][$priceList->id] = $priceList->title;
        }

        parent::__construct($model, OfferCategory::class);
    }
}
