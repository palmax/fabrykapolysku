@extends('default.layout')
@section('content')
    <div class="desktopFacebook">
        <button class="desktopFacebook__exit" onclick="closeFacebook()">
            <i class="fas fa-times"></i>
        </button>
        <button class="desktopFacebook__toggle" onclick="toggleFacebook()">
            Facebook
        </button>
        <span class="desktopFacebook__title">
            Zobacz nasze najnowsze realizacje! <br>
            Sprawdź naszego facebooka!
        </span>
        {!! getConstField('facebook_post') !!}

        @push('scripts.body.bottom')
            <script>
                setTimeout(() => {
                    jQuery('.desktopFacebook').addClass('-active');
                }, 5000)

                const toggleFacebook = () => {
                    jQuery('.desktopFacebook').toggleClass('-active');
                }

                const closeFacebook = () => {
                    jQuery('.desktopFacebook').removeClass('-active');
                }
            </script>
        @endpush
    </div>
    <div class="slider">
        <div class="autoplay">
            @foreach($page->gallery->items as $slide)
                @php
                    $img = renderImage($slide->url, 1920, 708, 'fit')
                @endphp
                <div class="slider__item">
                    <img src="{{$img}}" alt="">
                    <div class="text">
                        <h2 style="color:#fff">{!! str_replace(['</p>', '<p>'], '',  $slide->text) !!}</h2>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="prev"><img src="{{asset('images/back.png')}}" alt="Poprzedni"></div>
        <div class="next"><img src="{{asset('images/next.png')}}" alt="Następny"></div>
    </div>

    @push('scripts.body.bottom')

        <script>
            jQuery(document).ready(function () {
                jQuery('.autoplay').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 2000,
                    arrows: true,
                    dots: false,
                    fade: true,
                    prevArrow: jQuery('.prev'),
                    nextArrow: jQuery('.next'),
                    adaptiveHeight: false,
                    pauseOnHover: false,
                    pauseOnFocus: false,
                });
            })
        </script>
    @endpush

    <div class="section mcb-section bg-cover"
         style="padding-top:0px; padding-bottom:80px; background-color:#0f0d10; background-image:url({{asset('images/bg.jpg')}}); background-repeat:no-repeat; background-position:center top">
        <div class="section_wrapper mcb-section-inner">
            <div class="wrap mcb-wrap two-fifth  column-margin-0px valign-top move-up clearfix"
                 style="margin-top:-130px" data-mobile="no-up">
                <div class="mcb-wrap-inner">

                    <div class="column mcb-column one column_column" >
                        <div class="column_attr clearfix"
                             style=" padding:40px 25% 25px 40px;">
                            <!--
//style="background-image:url({{asset('images/box-bg.jpg')}}); background-repeat:no-repeat; background-position:center bottom; padding:40px 25% 25px 40px;"

                             <h6 class="sportsman-gradient" style="margin-bottom: 5px;">NASZA OFERTA</h6>
                            <hr class="no_line" style="margin:0 auto 10px">
                            <div style="color:#fff">
                                @include('default.offer.category.list')
                            </div>
        -->
                            {{--                            <hr class="no_line" style="margin:0 auto 10px">--}}
                            {{--                            <a class="button button_size_2 button_js" href="{{route('offer_category.index')}}"><span class="button_label">Zobacz</span></a>--}}
                        </div>
                    </div>
        -
                    <div class="column mcb-column one column_divider">
                        <hr class="no_line" style="margin:0 auto 30px">
                    </div>

                    <div class="column mcb-column one column_column">
                        <a href="{{getConstField('facebook_url')}}"  target="_blank"
                           class="column_attr clearfix icon-social"
                           style="background-image:url({{asset('images/facebook.png')}}); background-repeat:no-repeat;  display: block; background-position:left top; padding:40px 0 25px 130px; border-bottom: 1px solid #262129; background-position: 30px 50px;">
                            <h6 class="sportsman-gradient" style="margin-bottom: 5px;">FACEBOOK</h6>
                            <h3 style="color: #fff; margin-bottom: 0;"><span
                                    style="font-size: 16px;font-weight: 400;position: relative;top: -13px;">{{getConstField('facebook_name')}}</span>
                            </h3>
                        </a>
                    </div>
                    <div class="column mcb-column one column_column">
                        <a href="{{getConstField('instagram_url')}}" target="_blank"
                           class="column_attr clearfix icon-social"
                           style="background-image:url({{asset('images/instagram.png')}}); background-repeat:no-repeat; display: block; background-position:left top; padding:40px 0 25px 130px; background-position: 30px 50px;">
                            <h6 class="sportsman-gradient" style="margin-bottom: 5px;">INSTAGRAM</h6>
                            <h3 style="color: #fff; margin-bottom: 0;"><span
                                    style="font-size: 16px;font-weight: 400;position: relative;top: -13px;">{{getConstField('instagram_name')}}</span>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="wrap mcb-wrap three-fifth valign-top clearfix home-details-wrap" style="padding:80px 0 0 4%">
                <div class="mcb-wrap-inner">
                    <div class="column mcb-column one-sixth column_image">
                        <div class="image_frame image_item no_link scale-with-grid no_border">
                            <div class="image_wrapper">
                                <img class="scale-with-grid center-mobile" src="{{asset('images/line.png')}}">
                            </div>
                        </div>
                    </div>
                    <div class="column mcb-column five-sixth column_column">
                        <div class="column_attr clearfix">
                            <h2 class="headText" style="color:#fff; text-align: center;"><span
                                    class="sportsman-gradient">FABRYKA POŁYSKU</span> PROFESJONALNE STUDIO <br>
                                AUTODETALINGU <br>
                                <span style="margin-top: 1rem; margin-bottom: 2rem; display: flex; align-items: center; justify-content: space-between"><span>Detailing</span><span>PPF</span><span>Wrapping</span></span>
                            </h2>
                            <div class="contact">
                                <ul>
                                    <li>
                                        <a href="{{getConstField('google_map')}}" style="color: #fff; "><i
                                                class="fas fa-map-marker-alt"
                                                style="color: #ff0001"></i> {{getConstField('company_address')}}
                                            , {{getConstField('company_postcode')}} {{getConstField('company_city')}}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{getConstField('google_map_2')}}" style="color: #fff; "><i
                                                class="fas fa-map-marker-alt"
                                                style="color: #ff0001"></i> {{getConstField('company_address_2')}}
                                            , {{getConstField('company_postcode_2')}} {{getConstField('company_city_2')}}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="Tel:{{str_replace(' ', '', getConstField('phone'))}}"
                                           style="color: #fff; ">
                                            <i class="fas fa-phone-alt"
                                               style="color: #ff0001"></i> {{getConstField('phone')}}
                                        </a>
                                    </li>
                                    <li>
                                        <span style="color: #fff; "><i class="fas fa-check-square"
                                                                       style="color: #ff0001"></i> NIP: {{getConstField('company_nip')}}</span>
                                    </li>
                                    <li>
                                        <a href="Mailto{{getConstField('email')}}" style="color: #fff; "><i
                                                class="fas fa-envelope"
                                                style="color: #ff0001"></i> {{getConstField('email')}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="navIconsWrapper">
            <div class="section_wrapper mcb-section-inner">
                <div class="column mcb-column one-fourth column_column">
                    <div class="column_attr clearfix">
                        <a href="{{route('offer_category.index')}}" class="navIcon">
                            <img src="{{asset('images/car-service.png')}}" alt="" class="navIcon__icon">
                            <span class="navIcon__title">OFERTA</span>
                        </a>
                    </div>
                </div>
                <div class="column mcb-column one-fourth column_column">
                    <div class="column_attr clearfix">
                        <a href="{{route('realization.index')}}" class="navIcon">
                            <img src="{{asset('images/price-list.png')}}" alt="" class="navIcon__icon">
                            <span class="navIcon__title">CENNIK</span>
                        </a>
                    </div>
                </div>
                <div class="column mcb-column one-fourth column_column">
                    <div class="column_attr clearfix">
                        <a href="{{route('gallery.show')}}" class="navIcon">
                            <img src="{{asset('images/car.png')}}" alt="" class="navIcon__icon">
                            <span class="navIcon__title">GALERIA</span>
                        </a>
                    </div>
                </div>
                <div class="column mcb-column one-fourth column_column">
                    <div class="column_attr clearfix">
                        <a href="{{route('contact.show')}}" class="navIcon">
                            <img src="{{asset('images/location.png')}}" alt="" class="navIcon__icon">
                            <span class="navIcon__title">KONTAKT</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($fields->id_video_yt)
        <div class="section mcb-section" style="padding-top:80px; padding-bottom:30px;">
            <div class="section_wrapper mcb-section-inner">
                <div class="wrap mcb-wrap one valign-top clearfix">
                    <div class="ytVideo">

                        <iframe src="https://www.youtube.com/embed/{!! $fields->id_video_yt !!}?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="section mcb-section" style="padding-top:30px; padding-bottom:30px;">

        <div class="section_wrapper mcb-section-inner">
            <div class="wrap mcb-wrap one valign-top clearfix">
                <div class="mcb-wrap-inner">
                    <div class="facebook-wrap">
                        <div class="mobile">
                                 <span class="desktopFacebook__title">
            Zobacz nasze najnowsze realizacje! <br>
            Sprawdź naszego facebooka!
        </span>
                            <div>
                            {!! getConstField('facebook_post2') !!}
                            </div>
                        </div>
                    </div>
                    <div class="column mcb-column one column_column">
                        <div class="column_attr clearfix">
                            <h2><span class="sportsman-gradient"
                                      style="text-align: center; display: block">DLACZEGO MY?</span></h2>
                            <p class="big about-head" style="text-align: center">
                                Co nas wyróżnia?
                            </p>
                        </div>
                    </div>
                    <div class="column mcb-column one column_column">
                        <div class="column_attr clearfix">
                            <p class="about-text">
                                Jako jedyni w regionie prowadzimy w pełni profesjonalne, certyfikowane, akredytowane
                                studio aplikacji CERAMICZNYCH i GRAFENOWYCH powłok ochronnych oraz foli PPF
                            </p>
                            <span class="about-head">
                                Profesjonalny punkt zmiany koloru przez oklejenie
                            </span>
                            <p class="about-text">
                                Bezpłatna konsultacja, oględziny i wycena. <br><br>
                                Fachowa pomoc i doradztwo na najwyższym poziomie w miłej atmosferze. <br>
                                Pomożemy Ci wybrać najlepszy pakiet usług dopasowany indywidualnie do Twojego pojazdu
                                oraz zakresu cenowego.<br><br>

                                Na miejscu znajdą Państwo takie udogodnienia jak poczekalnia wyposażona w telewizor, PS4
                                czy ekspres do kawy.<br><br>

                                Nasi klienci bezpośrednio na miejscu mogą zakupić profesjonalne kosmetyki, zapachy i
                                akcesoria detalingowe.
                            </p>
                        </div>
                    </div>
                    <div class="about-items">
                        <div class="column mcb-column one-second column_column">
                            <div class="column_attr clearfix np-mobile" style="padding:0 15% 0 0;">
                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                    <div class="image_wrapper">
                                        <img class="scale-with-grid" src="{{asset('images/box-icons/five-stars.png')}}">
                                    </div>
                                </div>
                                <hr class="no_line" style="margin:0 auto 25px">
                                <h6 class="sportsman-gradient"
                                    style="text-transform: uppercase">{!! $fields->box1_1 !!}</h6>
                                <hr class="no_line" style="margin:0 auto 5px">
                                <p>
                                    {!! $fields->box1_2 !!}
                                </p>
                                <hr class="no_line" style="margin:0 auto 40px">

                            </div>
                        </div>
                        <div class="column mcb-column one-second column_column">
                            <div class="column_attr clearfix  np-mobile" style="padding:0 15% 0 0;">
                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                    <div class="image_wrapper">
                                        <img class="scale-with-grid" src="{{asset('images/box-icons/dollars.png')}}">
                                    </div>
                                </div>
                                <hr class="no_line" style="margin:0 auto 25px">
                                <h6 class="sportsman-gradient"
                                    style="text-transform: uppercase">{!! $fields->box2_1 !!}</h6>
                                <hr class="no_line" style="margin:0 auto 5px">
                                <p>
                                    {!! $fields->box2_2 !!}
                                </p>
                                <hr class="no_line" style="margin:0 auto 40px">

                            </div>
                        </div>
                            <div class="column_attr clearfix  np-mobile" style="padding:0 15% 0 0;">
                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                    <div class="image_wrapper">
                                        <img class="scale-with-grid" src="{{asset('images/box-icons/stopwatch.png')}}">
                                    </div>
                                </div>
                                <hr class="no_line" style="margin:0 auto 25px">
                                <h6 class="sportsman-gradient"
                                    style="text-transform: uppercase">{!! $fields->box3_1 !!}</h6>
                                <hr class="no_line" style="margin:0 auto 5px">
                                <p>
                                    {!! $fields->box3_2 !!}
                                </p>
                                <hr class="no_line" style="margin:0 auto 40px">
                            </div>
                        </div>
                        <div class="column mcb-column one-second column_column">
                            <div class="column_attr clearfix  np-mobile" style="padding:0 15% 0 0;">
                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                    <div class="image_wrapper">
                                        <img class="scale-with-grid" src="{{asset('images/box-icons/trust.png')}}">
                                    </div>
                                </div>
                                <hr class="no_line" style="margin:0 auto 25px">
                                <h6 class="sportsman-gradient"
                                    style="text-transform: uppercase">{!! $fields->box4_1 !!}</h6>
                                <hr class="no_line" style="margin:0 auto 5px">
                                <p>
                                    {!! $fields->box4_2 !!}
                                </p>
                                <hr class="no_line" style="margin:0 auto 40px">
                            </div>
                        </div>
                        <div class="column mcb-column one-second column_column">
                            <div class="column_attr clearfix  np-mobile" style="padding:0 15% 0 0;">
                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                    <div class="image_wrapper">
                                        <img class="scale-with-grid" src="{{asset('images/box-icons/free.png')}}">
                                    </div>
                                </div>
                                <hr class="no_line" style="margin:0 auto 25px">
                                <h6 class="sportsman-gradient"
                                    style="text-transform: uppercase">{!! $fields->box5_1 !!}</h6>
                                <hr class="no_line" style="margin:0 auto 5px">
                                <p>
                                    {!! $fields->box5_2 !!}
                                </p>
                                <hr class="no_line" style="margin:0 auto 40px">
                            </div>
                        </div>
                        <div class="column mcb-column one-second column_column">
                            <div class="column_attr clearfix  np-mobile" style="padding:0 15% 0 0;">
                                <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                    <div class="image_wrapper">
                                        <img class="scale-with-grid" src="{{asset('images/box-icons/guarantee.png')}}">
                                    </div>
                                </div>
                                <hr class="no_line" style="margin:0 auto 25px">
                                <h6 class="sportsman-gradient"
                                    style="text-transform: uppercase">{!! $fields->box6_1 !!}</h6>
                                <hr class="no_line" style="margin:0 auto 5px">
                                <p>
                                    {!! $fields->box6_2 !!}
                                </p>
                                <hr class="no_line" style="margin:0 auto 40px">
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    @include('default.article.category.home')
    @include('default.offer.category.other_home')
    {{--    <div class="container">--}}
    {{--        <div class="facebook-wrap">--}}
    {{--            {!! getConstField('facebook_post') !!}--}}
    {{--        </div>--}}
    {{--    </div>--}}
    @include('default.realization.category.slider')

@endsection


