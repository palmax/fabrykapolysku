@extends('default.layout')
@section('content')



    <section class="gallery">

        <div class="container">
            <div class="row justify-content-center">
                @foreach($page->gallery->items as $galleryItem)
                    <div class="col-md-6 col-lg-4">
                        <a href="{{renderImage($galleryItem->url, 1920, 1080, 'resize')}}">
                            <img src="{{renderImage($galleryItem->url, 500, 300, 'fit')}}" alt="">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    @include('default.realization.category.slider')


    @push('scripts.body.bottom')
        <script>
            var lightbox = jQuery('.gallery a').simpleLightbox({ /* options */ });
        </script>
    @endpush
@endsection
