<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_column">
                  <div class="column_attr clearfix">
                    <h2 style="font-size: 22px; line-height: 1.15; margin-bottom: 10px; margin-top: 1rem;"><span class="sportsman-gradient">Fabryka połysku Nowa Dęba</span></h2>
                    <div class="big" style="color:#000000">
                      <p style="padding-left: 0">Dzięki usługom oferowanym przez Fabrykę połysku w Nowej Dębie możesz całkowicie odmienić swój samochód! Zaufaj nam i ciesz się odświeżonym wyglądem samochodu. Nasze studio to miejsce, w którym powierzone nam samochody przechodzą zupełną metamorfozę. Jesteś ciekawy? Sam się przekonaj!</p>
                    </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_39_348ODemwt8qdYDD8TBodjioFXvpJaf3vOvI9enOd.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>


<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_19_dSM0g83ihuyDF6gdCzAro3UMTRuUv97BSc4iE5Ib.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
        <div class="mcb-wrap-inner">
            <div class="column mcb-column one column_column">
                <div class="column_attr clearfix">
                  <p><strong>Folia ochronna PPF Nowa Dęba – skutecznie zabezpieczymy Twój samochód</strong></p>

                  <p>Jak zabezpieczyć elementy samochodowe narażone na uszkodzenia? Do tego celu idealna będzie folia ochronna PPF, którą naklejamy, aby zabezpieczyć lakier przed otarciami, odpryskami i zarysowaniem. <strong>Zabezpieczenie lakieru w Nowej Dębie</strong> to najlepszy sposób na zachowanie idealnego stanu samochodu na długie lata. Zyskaj nieskazitelny wygląd pojazdu, korzystając z naszych profesjonalnych usług. Każdy klient może wybierać spośród dwóch rodzajów folii z połyskiem lub matowej. Każda z tych możliwości pozwoli na uzyskanie unikalnego efektu.</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
    <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_column">
            <div class="column_attr clearfix">
              <p><strong>Ceramiczne powłoki ochronne Nowa Dęba – skuteczne zabezpieczenie lakieru przed brudem</strong>&nbsp;</p>

              <p>Ciągłe woskowanie samochodu jest bardzo uciążliwe i pracochłonne, dlatego w naszym studio oferujemy usługę nakładania ceramiki, która zabezpieczy pojazd przed korozją i brudem, a także promieniowaniem ultra fioletowym. <strong>Ceramika na auto Nowa Dęba</strong> to najwyższej jakości powłoki ochronne, których celem jest kompleksowe zabezpieczenie lakieru samochodowego w o wiele większym stopniu, niż można je uzyskać przez zwykłe woskowanie. Przede wszystkim powłoki ceramiczne działają przez wiele miesięcy!</p>
            </div>
          </div>
      </div>
    </div>
    <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_image">
              <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                  <div class="image_wrapper">
                      <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_20_QIII5YtgjetulBHxoSEOdkonKDKlxDhUth1EIkK0.jpeg">
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_14_Fs9w6nOdWD9vgHKW2TJWmTLQDrxr34yHi6dRZ8Pm.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
        <div class="mcb-wrap-inner">
            <div class="column mcb-column one column_column">
                <div class="column_attr clearfix">
                  <p><strong>Polerowanie samochodu Nowa Dęba – usuwamy do 99% rys z lakieru samochodowego!</strong>&nbsp;</p>

                  <p>Rysy i przetarcia na karoserii dzięki naszym usługom odejdą w niepamięć. <strong>Polerowanie samochodu w Nowej Dębie</strong> pozwoli na wydobycie głębi koloru i uzyskanie nieskazitelnego blasku.</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
    <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_column">
            <div class="column_attr clearfix">
              <p><strong>Korekta lakieru Nowa Dęba</strong></p>

              <p>Oferowana przez nasze studio korekta lakieru pozwoli na usunięcie rys. Do wyboru każdego klienta, pozostawiamy kilka wariantów usług:</p>
              <ul>
                <li>jednoetapowa korekta lakieru  – pozwala na usunięcie 70% rys,</li>
                <li>dwuetapowa lub trzyetapowa korekta lakieru – polega na cięciu i polerowaniu, które usuwa 90% zarysowań,</li>
                <li>wieloetapowa korekta lakieru – szlifowanie, cięcie i polerowanie lakieru pozwala na usunięcie 99% rys,</li>
                <li>nabłyszczanie – standardowe nabłyszczenie usuwa nawet 50% rys.</li>
              </ul>
              <p>Jeśli chcesz przywrócić lakier do idealnego stanu, to skorzystaj z naszych profesjonalnych usług, które oferujemy w Fabryce Połysku.</p>
            </div>
          </div>
      </div>
    </div>
    <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
      <div class="mcb-wrap-inner">
          <div class="column mcb-column one column_image">
              <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                  <div class="image_wrapper">
                      <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_38_VikC0JjD2b1OYXSdfYjdFGrgEI6UzzFp9Wp0aKYe.jpeg">
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<div class="section mcb-section offer" style="padding-top:90px; padding-bottom:50px;">
  <div class="section_wrapper mcb-section-inner">
      <div class="wrap mcb-wrap one-second valign-top move-up clearfix" data-mobile="no-up">
          <div class="mcb-wrap-inner">
              <div class="column mcb-column one column_image">
                  <div class="image_frame image_item no_link scale-with-grid aligncenter no_border">
                      <div class="image_wrapper">
                          <img class="scale-with-grid" src="https://www.fabrykapolysku.pl/resized/780x1000_resize_public_gallery_item_47_fThHS0sqr9XUfu2CwdNBQRMKpUDDTLP8skD9KcD5.jpeg">
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="wrap mcb-wrap one-second no-padding-mobile valign-top clearfix" style="padding:0 4% 0 0">
        <div class="mcb-wrap-inner">
            <div class="column mcb-column one column_column">
                <div class="column_attr clearfix">
                  <p><strong>Autodetailing Nowa Dęba – profesjonalnie zadbamy także o wnętrze samochodu</strong>&nbsp;</p>

                  <p>Lśniący lakier bez najmniejszej rysy to nie wszystko. Zadbane wnętrze samochodu jest dla nas równie ważne, jak nieskazitelny stan karoserii, dlatego nasze usługi składają się również z kompleksowego czyszczenia wnętrza. <strong>Detailing Nowa Dęba</strong> to jedyny skuteczny sposób na uzyskanie idealnie czystego samochodu. Fabryka Połysku to miejsce, w którym świadczymy usługi autodetailingu w trzech wariantach do wyboru:</p>
                  <ul>
                    <li>podstawowy <strong>detailing Nowa Dęba</strong> – polega na czyszczeniu plastików, myciu szyb i wycieraczek, a także na odkurzaniu,</li>
                    <li>rozszerzony <strong>detailing Nowa Dęba</strong> – rozszerzenie usług z podstawowej oferty o: wydmuchiwanie zanieczyszczeń z zakamarków, czyszczenie, a także dressing plastików i wycieraczek,  czyszczenie drzwi i perfumowanie wnętrza,</li>
                    <li>pełny <strong>detailing Nowa Dęba</strong> – do wcześniejszych usług dołączone są: pranie podłogi, pranie tapicerki, pranie bagażnika, czyszczenie podsufitki, odkurzanie i ozonowanie.</li>
                  </ul>
                  <p>Fabryka Połysku w Nowej Dębie to profesjonalne miejsce, które oferuje swoim klientom najwyższy poziom usług. Nie zastanawiaj się i już teraz skorzystaj z naszej oferty, a my sprawimy, że Twój samochód będzie jak nowy!</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>