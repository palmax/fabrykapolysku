<?php


namespace App\Http\Controllers;


use App\Forms\Admin\PageForm;
use App\Http\Requests\PageRequest;
use App\Models\Article;
use App\Models\GalleryItem;
use App\Models\Offer;
use App\Models\Page;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;

class OfferController extends Controller
{
    public function show($item) {
        return view('default.offer.show', compact('item'));
    }

    public function index($view) {
        $items = Offer::with([])
            ->activeAndLocale()
            ->orderByDesc('position')
            ->paginate(5);
        $view->items = $items;
    }

    public function galleryHome($view) {
        $items =GalleryItem::with([])
            ->where('gallery_id', 3)
            ->where('active', 1)
            ->inRandomOrder()
            ->limit(4)
            ->get();
        $view->items = $items;
    }

}
