<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {!! SEOMeta::generate() !!}

    <link rel="canonical" href="{{url()->current()}}">

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('image/favicon2/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('image/favicon2/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('image/favicon2/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('image/favicon2/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('image/favicon2/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('image/favicon2/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('image/favicon2/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('image/favicon2/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('image/favicon2/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('image/favicon2/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('image/favicon2/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('image/favicon2/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('image/favicon2/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('image/favicon2//manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="facebook-domain-verification" content="sk0d9sk96pjrs9wd8lrnhmsszcm5gm" />

    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,900'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Rajdhani:100,300,400,400italic,500,700,700italic'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Kanit:100,300,400,400italic,500,700,700italic'>


    <link href="{{asset('css/vendors/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontawesome.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css"/>

    <link rel='stylesheet' href="{{asset('css/global.css')}}">
    <link rel='stylesheet' href="{{asset('css/structure.css')}}">
    <link rel='stylesheet' href="{{asset('css/sportsman.css')}}">
    <link rel='stylesheet' href="{{asset('css/simple-lightbox.min.css')}}">

    <link rel='stylesheet' href="{{asset('css/custom.css')}}">
    <link rel='stylesheet' href="{{asset('css/newCustom.css')}}?version=7">


{{--    <link href="{{asset('css/main.min.css')}}" rel="stylesheet">--}}
    <script>
        const BASE_URL = '{{url()->to('/')}}/';
        const CSRF_TOKEN = '{{csrf_token()}}';
        const SITE_LANG = '{{app()->getLocale()}}';
    </script>
    <!-- Meta Pixel Code -->
        {!! getConstField('meta_pixel_code') !!}
    <!-- End Meta Pixel Code -->

    <!-- Google tag (gtag.js) -->
        {!! getConstField('google_tag') !!}
    <!-- Google tag (gtag.js) -->

</head>

{{--<header style="max-width: 1360px; margin: 0 auto; border-bottom: 1px solid #ccc">--}}
{{--    header--}}


{{--    @include('default._helpers.lang_nav')--}}
{{--</header>--}}


{{--<main style="max-width: 1160px; margin: 0 auto; padding: 4rem 0">--}}


<body class="button-default layout-full-width no-content-padding header-transparent header-fw minimalist-header-no sticky-header sticky-tb-color ab-hide subheader-both-left menu-link-color menuo-right menuo-no-borders footer-copy-center mobile-tb-hide mobile-side-slide mobile-mini-mr-ll tablet-sticky mobile-sticky">
<script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>
<div id="Wrapper">
    <div id="Header_wrapper">
        <header id="Header">
            <div id="Top_bar">
                <div class="container">
                    <div class="column one">
                        <div class="top_bar_left clearfix">
                            <div class="logo">
                                <a href="{{route('index.show')}}"><img style="max-height: 80px!important;" src="{{asset('images/logo_new.png')}}" alt=""></a>
                            </div>
                            <div class="menu_wrapper">
                                <nav id="menu">
                                        @include('default.nav_item.main', ['name' => 'main'])
                                </nav>
                                <a class="responsive-menu-toggle" href="#"><i class="icon-menu-fine"></i></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>
        @if(!(Route::getCurrentRoute()->uri() == '/'))
        <div id="Subheader" style="padding:200px 0 20px">
            <div class="container">
                <div class="column one">
                    <h1 class="title">{{$fields->pageTitle ?? $pageTitle}}</h1>
                </div>
            </div>
        </div>
            @endif
    </div>
    <div id="Content">
        <div class="content_wrapper clearfix">
            <div class="sections_group">
                <div class="entry-content">
                  @if(Route::getCurrentRoute()->uri() == '/')
                    <div class="section mcb-section   hide-mobile" style="padding-top:0px; padding-bottom:0px; background-color:#0a0c0f">
                        <div class="section_wrapper mcb-section-inner">
                            <div class="wrap mcb-wrap one valign-top clearfix">
                                <div class="mcb-wrap-inner">
                                    <div class="column mcb-column one column_divider">
                                        <hr class="no_line" style="margin: 0 auto 120px;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  @endif


                  @switch(Route::getCurrentRoute()->uri())
                    @case('rzeszow')
                      @include('default.page.rzeszow')
                      @break
                    @case('debica')
                      @include('default.page.debica')
                      @break
                    @case('mielec')
                      @include('default.page.mielec')
                      @break
                    @case('sedziszow-malopolski')
                      @include('default.page.sedziszow-malopolski')
                      @break
                    @case('nowa-deba')
                      @include('default.page.nowa-deba')
                      @break
                    @default
                      @yield('content')
                  @endswitch
                </div>
            </div>

        </div>
    </div>

    <footer id="Footer" class="clearfix">

        <div class="footer_copy">
            <div class="container">
                <div class="column one city-menu">
                  <ul>
                    <li>
                      <a href="/rzeszow" style="color: #fff;">Rzeszów</a>
                    </li>
                    <li>
                      <a href="/debica" style="color: #fff;">Dębica</a>
                    </li>
                    <li>
                      <a href="/mielec" style="color: #fff;">Mielec</a>
                    </li>
                    <li>
                      <a href="/sedziszow-malopolski" style="color: #fff;">Sędziszów Małopolski</a>
                    </li>
                    <li>
                      <a href="/nowa-deba" style="color: #fff;">Nowa Dęba</a>
                    </li>
                  </ul>
                </div>
                <div class="column one">
                    <ul>
                        <li>
                            <a href="{{getConstField('google_map')}}" style="color: #fff; "><i class="fas fa-map-marker-alt" style="color: #ff0001; margin-right: .5rem"></i> {{getConstField('company_address')}}, {{getConstField('company_post_code')}} {{getConstField('company_city')}}</a>
                        </li>
                        <li>
                            <a href="{{getConstField('google_map_2')}}" style="color: #fff; "><i class="fas fa-map-marker-alt" style="color: #ff0001; margin-right: .5rem"></i> {{getConstField('company_address_2')}}, {{getConstField('company_post_code_2')}} {{getConstField('company_city_2')}}</a>
                        </li>
                        <li>
                            <a href="Tel:{{str_replace(' ', '', getConstField('phone'))}}" style="color: #fff; ">
                                <i class="fas fa-phone-alt" style="color: #ff0001; margin-right: .5rem"></i> {{getConstField('phone')}}
                            </a>
                        </li>
                        <li>
                            <span style="color: #fff; "><i class="fas fa-check-square" style="color: #ff0001; margin-right: .5rem"></i> NIP: {{getConstField('company_nip')}}</span>
                        </li>
                        <li>
                            <a href="Mailto:{{getConstField('email')}}" style="color: #fff; "><i class="fas fa-envelope" style="color: #ff0001; margin-right: .5rem"></i> {{getConstField('email')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="column one">
                    <div class="copyright">
                      Strona stworzona przez: <a target="_blank" rel="nofollow" href="https://www.palmax.com.pl">Palmax</a>
                    </div>

                </div>
            </div>
        </div>
    </footer>

</div>
{{--</main>--}}
<div id="Side_slide" class="right dark" data-width="250">
    <div class="close-wrapper">
        <a href="#" class="close"><i class="icon-cancel-fine"></i></a>
    </div>
    <div class="menu_wrapper">

    </div>
</div>
<div id="body_overlay"></div>

<div class="contactFixed">
    <a href="{{getConstField('facebook_url')}}" target="_blank"><img src="{{asset('images/facebook.png')}}" alt=""></a>
    <a href="{{getConstField('instagram_url')}}" target="_blank"><img src="{{asset('images/instagram.png')}}" alt=""></a>
    <a href="Tel:{{str_replace(' ', '', getConstField('phone'))}}"><img src="{{asset('images/phone.png')}}" alt=""></a>
    <a href="Mailto:{{getConstField('email')}}"><img src="{{asset('images/email.png')}}" alt=""></a>
</div>

<script src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('js/all.min.js')}}"></script>
<script src="{{asset('js/mfn.menu.js')}}"></script>
<script src="{{asset('js/jquery.plugins.js')}}"></script>
<script src="{{asset('js/jquery.jplayer.min.js')}}"></script>
<script src="{{asset('js/animations.js')}}"></script>
<script src="{{asset('js/translate3d.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{asset('js/simple-lightbox.jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>

<script src="{{asset('js/email.js')}}"></script>
<script src="{{asset('js/frontend.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

{{--<script src="{{asset('js/main.min.js')}}"></script>--}}
<script>
    jQuery('.logo img').css('height', 80+ 'px')

    jQuery(document).on('scroll', function (){

        let scrollTop = jQuery(document).scrollTop()
        let bar = jQuery('#Top_bar').outerHeight()
        if (scrollTop < bar){
            jQuery('.logo img').css('height', 136+ 'px')
        } else {
            jQuery('.logo img').css('height', bar-10 + 'px')
        }
   })
</script>
<script>
    setTimeout(()=> {
        AOS.init();
    }, 1000)
</script>
@stack('scripts.body.bottom')
</body>
</html>
